<?php

    session_start();

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="fonts/font.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Projeto CRUD</title>
</head>
<body>

    <main class="container">
        <form class="login-wrapper" action="servidor/cadastro.php" method="POST">
            <h1>Cadastro</h1>
            <div class="login-inputs">

                <div class="erros"></div>

                <label for="email-input"  class="label1">E-mail</label>
                <input type="mail" id="email-input" placeholder="Digite seu e-mail" name="email">

                <label for="password-input" class="label1">Senha</label>
                <input type="password" id="password-input" placeholder="Digite sua senha" name="password">

                <label for="password-input" class="label1">Repita a senha</label>
                <input type="password" id="password-input" placeholder="Repita sua senha" name="passwordr">

                <input type="submit" value="Entrar" id="submit-input" name="register">
            </div>
            <div class="side-links">
                <a href="entrar-pagina.php" class="link1">Já tenho conta</a>
                <a href="#" class="link1">Ajuda</a>
            </div>

        </form>
    </main>
</body>
</html>