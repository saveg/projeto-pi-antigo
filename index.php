<?php

session_start();

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Projeto CRUD</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/main.js"></script> <!--Defer = carrega depois do Body-->

    <link rel="stylesheet" href="fonts/font.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

    <main class="container">

    <?php
        if(isset($_SESSON['email'])){
            echo "<h1>Hello, </h1>
                        <a href='logout.php'>Sair<a/>";
        }
        else{
            echo "<h1>Para ver este conteúdo você deve se logar primeiro.</h1>
                        <a href='entrar-pagina.php'>Entrar<a/>";
        }
    ?>

    </main>
</body>
</html>