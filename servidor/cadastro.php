<?php

if (isset($_POST['register'])){

    require 'connect.php';

    $email = $_POST['email'];
    $pass = $_POST['password'];
    $passr = $_POST['passwordr'];

    if(empty($email) || empty($pass) || empty($passr)){
        header("Location: ../cadastro-pagina.php?error=emptyfields&email=" .$email);
        exit();

    }  

    else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        header("Location: ../cadastro-pagina.php?error=invalidmail&email=".$email);
        exit();
    }

    else if ($password !== $passwordr){
        header("Location: ../cadastro-pagina.php?error=passwordcheck&email=".$email);
        exit();
    }

    else{

        $sql = "SELECT email FROM users WHERE email=?";

        $stmt = mysqli_stmt_init($conn);

        if(!mysqli_stmt_prepare($stmt, $sql)){
            header("Location: ../cadastro-pagina.php?error=sqlerror");
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, "s", $email);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);

            $resultCheck = mysqli_stmt_num_rows($stmt);

            if($resultCheck > 0){
                header("Location: ../cadastro-pagina.php?error=emailtaken&mail=".$email);
                exit();
            }
            else{

                $sql = "INSERT INTO users (email, password) VALUES (?, ?)";
                $stmt = mysqli_stmt_init($conn);

                if(!mysqli_stmt_prepare($stmt, $sql)){
                    header("Location: ../cadastro-pagina.php?error=sqlerror");
                    exit();

            }else{
                // bcrypt
                $hashPassword = password_hash($password, PASSWORD_DEFAULT);

                mysqli_stmt_bind_param($stmt, "ss", $email, $hashPassword);
                mysqli_stmt_execute($stmt);
                header("Location: ../entrar-pagina.php?signup=success");
                exit();
            }
        }

    }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);

}

}
else{

    header("Location: ../cadastro-pagina.php");
    exit();

}
