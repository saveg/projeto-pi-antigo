<?php

/* AJAX Login Validation*/
// $email = isset($_POST['email']) ? $_POST['email'] : '';
// $senha = isset($_POST['password']) ? $_POST['password'] : '';

// if ($email == 'vinicius@gmail.com' && $senha == '123'){

//     echo (1);

// }else{

//     echo (0);
/* AJAX Login Validation*/


if(isset($_POST['submit'])){

    require 'connect.php';

    $email = $_POST['email'];
    $password = $_POST['password'];

    if(empty($email) || empty($password)){
        header("Location: ../entrar-pagina.php?error=emptyfields");
        exit();
    }

    else{

        $sql = "SELECT * FROM users WHERE email=?";

        $stmt = mysqli_stmt_init($conn);

        if(!mysqli_stmt_prepare($stmt, $sql)){
            header("Location: ../entrar-pagina.php?error=sqlerror");
            exit();
        }
        else{
            mysqli_stmt_bind_param($stmt, 's', $email);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);

            if($row = mysqli_fetch_assoc($result)){

                $passwordCheck = password_verify($hashpassword, $row['password']);

                if($passwordCheck == false){

                    header("Location: ../entrar-pagina.php?error=wrongpassword");
                    exit();

                }
                else if($passwordCheck == true){
                    session_start();
                    $_SESSION['email'] = $row['email'];

                    header("Location: ../index.php?login-success");
                    exit();
                }

            }
            else{
                header("Location: ../entrar-pagina.php?error=noemail");
                exit();
            }
        }

    }

}

else{
    header("Location: ../entrar-pagina.php");
    exit();
}

?>