<?php

    session_start();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Projeto CRUD</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script defer src="js/ajax.js"></script> Defer = carrega depois do Body -->

    <link rel="stylesheet" href="fonts/font.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>

    <main class="container">
        <form class="login-wrapper" id="login-form" action="servidor/entrar.php" method="POST">
            <h1>Entrar</h1>
            <div class="login-inputs">

                <div class="error"></div>

                <label for="email-input"  class="label1">E-mail</label>
                <input type="mail" id="email-input" placeholder="Digite seu e-mail" name="email" required>

                <label for="password-input" class="label1">Senha</label>
                <input type="password" id="password-input" placeholder="Digite sua senha" name="password" required>

                <input type="submit" value="Entrar" id="submit-input" name="submit">
            </div>
            <div class="side-links">
                <a href="cadastro-pagina.php" class="link1">Não tenho conta</a>
                <a href="#" class="link1">Esqueci minha senha</a>
            </div>

        </form>
    </main>
</body>
</html>