$(document).ready(function(){
  
    // Form ID 
    var login = $("#login-form");
    
    // Variable for AJAX output response
    var error = $(".error");

    login.on('submit', function(e){
        e.preventDefault();
        
        // JSON dados
        var data = $('#login-form').serialize();

        // AJAX Function
        $.ajax({

            // PHP File Location
            url: './servidor/entrar.php',
            // DATA wich will be sent
            type: 'POST',
            dataType: 'json',
            data: data,
            // Before response:
            beforeSend: function(){
                error.html('Carregando...');
            },

            // Succes data sent:
            success: function(response){
                console.log(response);
                if(response == 1){
                    error.html('Entrou');
                    window.location.href = "./index.php";
                }else{
                    error.html('E-mail/Senha inválidos');
                }       
                
            },
            error: function(e){
                console.log(e.responseText);
            }

        });

    });

});